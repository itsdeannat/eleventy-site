# deanna.dev

Deanna Thompson's personal website for all things code, technical writing, and well...anything else.

## Built with 

* Eleventy
* Netlify

## Authors

[Deanna Thompson](https://twitter.com/itsdeannat)

[![Netlify Status](https://api.netlify.com/api/v1/badges/93f17bfb-b4c7-4f8a-b47a-3fd3f8c89551/deploy-status)](https://app.netlify.com/sites/suspicious-mahavira-33ed25/deploys)