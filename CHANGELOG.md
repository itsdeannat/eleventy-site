# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.2.5](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.2.4...v1.2.5) (2022-04-14)


### Housekeeping items

* Fix broken link to my GitLab account [skip ci] ([1f3b7f1](https://gitlab.com/itsdeannat/eleventy-site/commit/1f3b7f14d548fcb6a402cefe0ec780dcbcb99296))

### [1.2.4](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.2.3...v1.2.4) (2022-03-17)


### Housekeeping items

* Update README and site.json [skip ci] ([ed0318b](https://gitlab.com/itsdeannat/eleventy-site/commit/ed0318b99f6472d8207389154d2b16d88468bf66))

### [1.2.3](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.2.2...v1.2.3) (2022-03-15)


### Housekeeping items

* Update contact form to capture user name and email [skip ci] ([177e82e](https://gitlab.com/itsdeannat/eleventy-site/commit/177e82ec28557ec4f07c1aff8f069bd41c47139c))

### [1.2.2](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.2.1...v1.2.2) (2022-03-12)


### New content

* Add contact page [skip ci] ([5dedf2e](https://gitlab.com/itsdeannat/eleventy-site/commit/5dedf2e5eeb3b4b1d4aa40394f2d839538c5a21d))


### Housekeeping items

* Update contact page title [skip ci] ([93869f5](https://gitlab.com/itsdeannat/eleventy-site/commit/93869f576347577444e275ae2d55af514ffa0d4a))
* Update file to include new contact page ([2f9e789](https://gitlab.com/itsdeannat/eleventy-site/commit/2f9e7894eadbed6049bfc33345ac28740a4cf7bf))
* Update homepage tab title ([1fbec2d](https://gitlab.com/itsdeannat/eleventy-site/commit/1fbec2d083d6ae5cbeeda94a838ab1b302150f54))

### [1.2.1](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.2.0...v1.2.1) (2022-03-12)


### Housekeeping items

* Change website tagline [skip ci] ([01c06ae](https://gitlab.com/itsdeannat/eleventy-site/commit/01c06ae22171b5a8861ea486c5156145c4dd13a2))

## [1.2.0](https://gitlab.com/itsdeannat/eleventy-site/compare/v1.1.0...v1.2.0) (2022-03-12)


### Design updates

* Update global css file ([cd700bc](https://gitlab.com/itsdeannat/eleventy-site/commit/cd700bc0c2fad39044e14a4c945843161bc9c522))
* Update prism-theme css file ([5facdb0](https://gitlab.com/itsdeannat/eleventy-site/commit/5facdb0f67707ccae52801e1b5f28c963f31a870))
* Update variable.css ([b001acd](https://gitlab.com/itsdeannat/eleventy-site/commit/b001acd1968022179b8463facb9683d66b038e00))


### New content

* Add tutorial on standard-version [skip ci] ([ba4151c](https://gitlab.com/itsdeannat/eleventy-site/commit/ba4151c30c51f9f6b9e18d16d4f5043a85baca45))

## 1.1.0 (2022-02-24)


### Build process updates

* Update package.json file ([f34eea4](https://gitlab.com/itsdeannat/eleventy-site/commit/f34eea4924e03f37304ee63a4cd8619a1c775086))


### New content

* Add blog posts ([eea8563](https://gitlab.com/itsdeannat/eleventy-site/commit/eea8563bf3b2897ec5d97d834c335f8adbd01e58))
* Add new content to home and about pages ([43fe491](https://gitlab.com/itsdeannat/eleventy-site/commit/43fe491ada71d2e2eb6f4f033db2ad9708f5d41c))
* Add work page ([c5b6c93](https://gitlab.com/itsdeannat/eleventy-site/commit/c5b6c93bae3dbe7d16e66a7199b6658aaa3e1be3))
* Update profile picture ([dc653d3](https://gitlab.com/itsdeannat/eleventy-site/commit/dc653d3b2c745b9fae0e648030da834f50cfa164))


### Housekeeping items

* Add .versionrc file ([7ce8e39](https://gitlab.com/itsdeannat/eleventy-site/commit/7ce8e39f0aac132c0bd10780944effd5bf9e748b))
* Add images for static site post ([139c948](https://gitlab.com/itsdeannat/eleventy-site/commit/139c9489e7ffc0e7303398352038eebd866d978f))
* Add package-lock.json file ([7863678](https://gitlab.com/itsdeannat/eleventy-site/commit/7863678358284ba74762506451bb7182c50f67a3))
* Change site title and social links ([45f5bf7](https://gitlab.com/itsdeannat/eleventy-site/commit/45f5bf75a1bce0388f1a5957458213dc1dfbf4ec))
* Delete package-lock.json [skip ci] ([e4dd38b](https://gitlab.com/itsdeannat/eleventy-site/commit/e4dd38bcad51b7cdd6678133f6589168b695579c))
* Delete starter blog posts ([f5b81c9](https://gitlab.com/itsdeannat/eleventy-site/commit/f5b81c914c5c1e32f75094e8e280d0e9e6c163c3))
* Modify margins for images and h1 level headings ([b7cfec3](https://gitlab.com/itsdeannat/eleventy-site/commit/b7cfec3da45ffd90b139e322011f2756b2b1c315))
* Update copyright footer ([3e7588c](https://gitlab.com/itsdeannat/eleventy-site/commit/3e7588c393b3ae780423361d8cdd26b83c3cede9))
* Update site.json to mention new work page ([51be509](https://gitlab.com/itsdeannat/eleventy-site/commit/51be5097630e443e433db1d612c3e9b936a1313f))
