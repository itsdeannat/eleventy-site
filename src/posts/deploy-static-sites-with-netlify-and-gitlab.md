---
title: "Setting up static sites with Netlify CLI and GitLab"
description: "I couldn't use the Netlify web UI to deploy my site. To the CLI I go."
tags: [netlify, gitlab, static sites, deployments]
date: '2021-08-20'
---

Yesterday, I found myself in a bit of a conundrum. I migrated the repository for this site from GitHub to GitLab because I've been using GitLab lately and like it. Because I'm using a new Git provider, I needed to set up a new site from Git using Netlify's UI. But every time I tried to deploy my site, I kept getting a "Looks like something went wrong!" error that really didn't explain *what* was wrong. 

The solution, according to Netlify's support team, was to link my repository using Netlify CLI, not the UI. I still don't know why it happened, but it happened.

Thankfully, it all worked out. I want to share how I set up my static site using the CLI if someone out there runs into the same problem. 

> Assumptions I'm making: 
> 
> * You have a Netlify account and a Git repository hosted on GitLab.
> * Node.js and npm are installed on your machine.

## Step 1 — Install Netlify CLI

First, you need to install Netlify CLI. Run this command in your terminal: `npm install netlify-cli -g`. 

> If you get a "permission denied" error, it's because npm can't access/write to the `usr/local/lib/node_modules` directory. Check out this [blog post](https://www.willandskill.se/en/how-to-fix-eacces-errors-with-npm-on-macos/) from Will & Skill about resolving this error.

To verify installation, run `netlify -v`.

## Step 2 — Obtain a token through the command line

To authenticate and get an access token through the command line, run the command `netlify login`. You'll be asked to log in with Netlify to grant access to Netlify CLI.

## Step 3 — Configure continous deployment for your site

To connect your GitLab repository, run the command `netlify init --manual` from your local repository. This command configures continuous deployment for your site. 

Next, the CLI asks you if you want to connect the directory to an existing Netlify site or create and configure a new site. Make sure to select "Create & configure a new site."

After that, the CLI walks you through several steps:

1. Selecting your team and site name
2. Entering a build command for your site 
3. Setting the directory to deploy
5. Creating a [Netlify functions](https://docs.netlify.com/functions/overview/) directory
6. Creating a `netlify.toml` file, or Netlify's configuration file

After you complete these steps, the CLI prompts you for your deploy settings, then provides you with two items you need to add to your repository settings in GitLab. The first is a deploy key, and the second is a webhook. 

### Add a deploy key to your project

The deploy key looks something like this:

```
ssh-rsa AAAAAdkdh67x/lskdjfjTTIKGN585:lskdhfsnxnnrnOXNSNWMloiahwl8WE7R7E+4k...
```

Copy the key, and then open your project in GitLab. In the sidebar, go to **Settings > Repository**. Expand the **Deploy keys** section.

<figure>
    <center><img src="/images/deploy_key.png" width=600 alt="Deploy Key in GitLab"></center>
    <center><figcaption>Screenshot of the Deploy keys section in the Gitlab UI</figcaption></center>
</figure>

Give the key a title, like "Netlify" or the name of your site. Then paste the deploy key in the **Key** box. Click **Add key** to finish this task.

### Add a GitLab webhook

Next, the CLI prompts you to configure a webhook. Webhooks listen for specific events like pushes, issues, or merge requests. GitLab sends a `POST` request with data to the webhook URL.

The webhook URL looks something like this:

```
https://api.netlify.com/hooks/XXXXXXXXXXXXXXXXX
```

In GitLab, go to **Settings > Webhooks**. 

<figure>
    <center><img src="/images/webhooks.png" width=600 alt="Webhooks in GitLab"></center>
    <center><figcaption>Image of the Webhooks page in the GitLab UI</figcaption></center>
</figure>


In the **URL** box, paste the webhook URL. If you want, you can specify a secret token.

In the **Trigger** section, you can select the events that are triggered when a certain action occurs, like pushing to a specific branch, creating a new issue, or updating a merge request. Then, click **Add webhook**.

## Step 4 — Test deployment

When prompted to continue in the terminal, press **enter**. You'll see a success message indicating that your site is configured to deploy from GitLab. 

The last thing to do is push to your repository to trigger a new site build. Make a small change to a file in your repository, and then run `git push`. 

Log in to your Netlify account again. In the **Sites** section, you'll see your new site. Click on it to see your site's details.

You can see if your site deployed successfully in the **Production deploys** section. That's it!

## Troubleshooting

If your deployment fails, here are a few things to try:

1. Make sure your build command and publish directory are correct.
2. Make sure your site builds locally. 

See Netlify's [Build troubleshooting tips](https://docs.netlify.com/configure-builds/troubleshooting-tips/) in the Netlify Docs for more help.

## Final thoughts

Overall, the process of creating a new site through the CLI wasn't too difficult. The [Netlify Docs](https://docs.netlify.com/) are thorough and provided me with almost everything I needed. The only problem I encountered was the permissions error with npm. 