---
layout: base
title: "Work"
---

Since becoming a technical writer, I've worked on several projects, including style guide development and implementing docs-as-code methodologies to help developers write quality documentation.

## Style guide management at Tech Elevator

The development team I worked with adopted the Google Developer Documentation Style Guide (GDDSG) as their default style guide in 2018. It served their needs for the most part, but over time, I noticed that it lacked guidance in several areas. 

Because the GDDSG didn't provide us with the guidance we needed, our documentation remained inconsistent at times. We'd also make documentation decisions during our team meetings, but we had nowhere to document these decisions.

The solution was to create a customized style guide for my team, but one that only covered what we needed it to.

If you'd like to learn more about how I built a customized style guide for my team, you can watch my presentation at Write the Docs Portland 2021: *Building a style guide from the ground up: lessons learned from a lone writer*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PkK1lowfeFU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br />

## Testing tutorials

As the user advocate, I regularly test all tutorials to make sure there are no gaps in the material, that the instructions are easy to follow, and most importantly, that they *work*. When testing tutorials, I put myself into the user's shoes as much as possible.

When testing tutorials, I check for the following:

* **That we're not making any assumptions**: sometimes we assume users know where some UI element is, or understand what some command means when we haven't given them proper context.
* **That the expected result and actual result are the same**: if the tutorial instructions say that users should see a confirmation message after submitting a form but they don't, that means the instructions have failed.
* **That code samples make sense**: this involves checking for typos, making sure we aren't missing any `using` or `import` statements, or that our method signatures are correct.

## Implementing linters for documentation

To help the developers at Tech Elevator write with a consistent writing style, I implemented [Vale](https://errata.ai), an open-source grammar and style linter, for our documentation repositories. Vale is highly customizable and supports creating custom tests that you can run against your writing.

Implementing the linter involved adding it to our repositories, running it against existing content, and then editing that content so it adhered to our style guide conventions. When I ran Vale against our content for the first time, it reported many errors (typos, punctuation errors), warnings (terminology, word choice), and suggestions (passive voice, use of parentheses).

After two months of work, I reduced the amount of errors by 56% and the amount of warnings 50% for one repository. Errors in another repository decreased 86% and warnings 36%.

As our team grew, I eventually moved us from using the command-line tool to [Vale Server](https://docs.errata.ai/vale-server/install/), a desktop application with functionality similar to the CLI, but with features that help you manage multiple projects and configurations.