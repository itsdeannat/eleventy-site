---
layout: base
title: 'Contact'
---

# Contact me

Contact me with any questions! I'll try to get back to you as soon as I can.

<p></p>

<style>
    input, button, textarea {
        display: block;
        width: 50%;
        margin: 10px 0;
        padding: 10px;
    }

    .name-input {
        border-radius: 10px;
        border: 3px solid #eee;
        transition: .3s border-color;
    }

    .name-input:hover {
        border: 3px solid #bebebe;
    }

    .email-input {
        border-radius: 10px;
        border: 3px solid #eee;
        transition: .3s border-color;
    }

    .email-input:hover {
        border: 3px solid #bebebe;
    }

    .message-input {
        border-radius: 10px;
        border: 3px solid #eee;
        transition: .3s border-color;
        width: 75%;
    }

    .message-input:hover {
        border: 3px solid #bebebe;
    }

    button {
        padding: 10px;
        border: none;
        color: #000000;
        font-weight: 600;
        border-radius: 5px;
        width: 25%
    }
</style>

<div class="contact-form">
<form id="myForm" action="https://formspree.io/f/xleawakr" method="POST">
  <label>
    <input type="text" name="name" class="name-input" placeholder="Name">
  </label>
  <label>
    <input type="text" name="email" class="email-input" placeholder="Your email" />
  </label>
  <label>
    <textarea name="message" class="message-input" placeholder="Your message"></textarea>
  </label>
<!-- your other form fields go here -->
  <button type="submit">Send</button>
</form>
</div>
