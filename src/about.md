---
title: About 
layout: about.njk
name: me
image: '/images/deanna-thompson.png'
---

I'm a technical writer with experience in using docs-as-code and topic-based authoring approaches to writing and managing documentation.

Before I became a technical writer, I taught developmental writing at various colleges in the Cleveland area. And before that, I studied linguistics for six years. My work as a Technical Writer blends my years of experience as an English teacher, working with learners of all ages, and an interest in programming!

# Skills and expertise

* **Markup languages**: Markdown, Asciidoc
* **Structured authoring**: DITA, Oxygen XML Author
* **Version control**: Bitbucket, GitLab, GitHub
* **Programming languages**: C#/.NET, Python 
* **Project management**: Jira, Confluence, Asana, Agile Scrum
* **Static Site Generators**: VuePress, Jekyll, Hugo
* **Technical experience in DevOps and QA**: CI/CD, testing instructions, process automation
* Experience writing tutorials, release notes, training materials, and managing style guide development

